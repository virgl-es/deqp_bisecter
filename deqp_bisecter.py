#!/usr/bin/python3

import argparse
import os
import sys
import subprocess

def runTest(testName, startIdx):
    ret = 0
    glctsPath = args.list + "/../../../../../../.."
    cmd = "cd " + glctsPath + "; ./glcts --deqp-case={}".format(testName)
    output = subprocess.getoutput(cmd).split("DONE!")[1]

    if "Failed:        1/" in output:
        ret = 1

    print("#{} {} {}".format(list.index(testName),
                             "SUCCESS" if ret == 0 else "FAILURE",
                             testName))

    return ret


def runTests(startIdx, targetIdx):
    if startIdx < 0:
        startIdx = 0

    subList = list[startIdx:targetIdx+1];
    print("Running tests #{} - #{}, {} tests".format(startIdx, targetIdx, len(subList)))
    ret = None
    for test in subList:
        ret = runTest(test, startIdx)

    return ret



def iterativeTesting(targetIdx):
    if (targetIdx <= 0):
        print("The target test is the first one, which means no previous test could have caused its failure.")
        sys.exit()

    ret = 0
    prevStartIdx = targetIdx
    startIdx = targetIdx
    testSpan = 1

    # Look for a startIdx where targetTest starts to fail
    while ret == 0 and startIdx >= 0:
        prevStartIdx = startIdx
        startIdx = targetIdx - testSpan;
        ret = runTests(startIdx, targetIdx)
        testSpan *= 2

    if startIdx < 0:
        startIdx = 0

    if ret == 0:
        print("No tests seems to cause the target test to fail.")
        sys.exit()

    # We've found a span which causes failures,
    # let's try to remove tests from the front that
    # aren't causing the issue
    failSpanStartIdx = startIdx
    failSpanEndIdx = prevStartIdx - 1
    print("Failure found using the #{} - #{} tests".format(startIdx, targetIdx))
    print("The failure was introduced between #{} - #{}".format(failSpanStartIdx, failSpanEndIdx))
    for startIdx in reversed(range(failSpanStartIdx, failSpanEndIdx + 1)):
        ret = runTests(startIdx, targetIdx)
        if ret != 0:
            break
    print("The failure was introduced at #{} - {}".format(startIdx, list[startIdx]))


def replicateState(targetTest):
    targetIdx = list.index(targetTest)
    print("Replicating state leading failure of test #{} - {}".format(targetIdx, targetTest));
    iterativeTesting(targetIdx);


def loadTestsList(excludeList):
    global list
    list = []
    
    listFile = os.path.expanduser(args.list);
    with open(listFile) as f:
        content = f.readlines()

    content = [x.strip() for x in content]
    
    excludeCtr = 0
    for line in content:
        includeLine = True
        
        for excludeLine in excludeList:
            if line.startswith(excludeLine):
                includeLine = False
        
        # Never exclude the test we're looking for
        if line == args.test:
            includeLine = True

        if not includeLine:
            excludeCtr += 1
        
        if includeLine:
            list.append(line)

    print("{} tests loaded, {} excluded ".format(len(list), excludeCtr));


def parseArgs():
    global args
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="Failing test to search for")
    parser.add_argument("--list", help="Path to list of tests to run")
    args = parser.parse_args()


def main():
    parseArgs()
    
    excludeList = []
    loadTestsList(excludeList)
    print("{}".format(list))


    replicateState(args.test)


if __name__ == "__main__":
    main()



