deqp_bisecter
=============

This tools is meant to be used to find tests that are causing _other_ tests to fail.

So for example if test #3 is putting the graphics driver into a bad state
which will cause test #9 to fail, we want to be able to figure out that
test #3 is the cause.


How does it work
----------------

### Build OpenGL/Vulkan test suite

Instructions based on the following document:
https://github.com/KhronosGroup/VK-GL-CTS/blob/master/external/openglcts/README.md

    git clone https://github.com/KhronosGroup/VK-GL-CTS.git
    cd VK-GL-CTS
    mkdir build
    cd build
    cmake .. -DDEQP_TARGET=x11_egl -DGLCTS_GTF_TARGET=gles2
    cmake --build .


### Run this tool

    deqp_bisecter.py --list=VK-GL-CTS/build/external/openglcts/modules/gl_cts/data/mustpass/gles/aosp_mustpass/master/gles2-master.txt --test=dEQP-GLES2.functional.fragment_ops.interaction.basic_shader.5
